#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm1 = Adafruit_PWMServoDriver(0x40);
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41);

#define SERVOMIN  150 // минимальная длительность импульса для сервомашинки 150
#define SERVOMAX  350 // максимальная длина импульса для сервомашинки 600
#define SERVOPORT 2 // Номер порта для подключения сервомашинки

int16_t dec = -32767;
int16_t one = -32767;
int16_t temperature = 88;  // −32767.. +32767
uint8_t sign = 0;
uint8_t weather = 0; 
String data;

#define PIN_SWITCH_1 13

void setup() 
{
  Serial.begin(115200);
  pinMode (PIN_SWITCH_1, OUTPUT);
  pwm1.begin();
  pwm1.setPWMFreq(60);  // Частота следования импульсов 60 Гц
  delay(10);
  pwm2.begin();
  pwm2.setPWMFreq(60);  // Частота следования импульсов 60 Гц
  delay(10);  
  for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
      for(int i = 0; i < 16; i++) {
          pwm1.setPWM(i, 0, pulselen);
          pwm2.setPWM(i, 0, pulselen);}  
  }
  for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
      for(int i = 0; i < 16; i++) {
          pwm1.setPWM(i, 0, pulselen);
          pwm2.setPWM(i, 0, pulselen);}  
  }
}
void loop() 
{ 
  if (Serial.available()>0) {
    data = Serial.readString();
    temperature = data.toInt(); 
    if (temperature < 0) {
      for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) pwm1.setPWM(13, 0, pulselen);
      for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) pwm1.setPWM(12, 0, pulselen);
    } else {
      for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) pwm1.setPWM(13, 0, pulselen);
      for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) pwm1.setPWM(12, 0, pulselen);
    }
    temperature = abs(temperature);
    if (one != temperature%10){
    switch (temperature % 10){
       case 0:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(3, 0, pulselen);}               
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}               
                delay(250);
                one = 0;
                break;}  
       case 1:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);}                
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}                
                delay(250);
                one = 1;
                break;}            
       case 2:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(6, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);}
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);}
                delay(250);
                one = 2;
                break;}
       case 3:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);}
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(6, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(0, 0, pulselen);}
                delay(250);
                one = 3;
                break;}           
       case 4:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(5, 0, pulselen);
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);}
                delay(250);
                one = 4;
                break;}
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}
                delay(250);  
       case 5:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);}
                delay(250);  
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);}
                delay(250);
                one = 5;
                break;}
       case 6:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(1, 0, pulselen);}
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}
                delay(250); 
                one = 6;            
                break;}
       case 7:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);}
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}
                delay(250);
                one = 7;
                break;}
       case 8:{
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(4, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}
                delay(250);
                one = 8;
                break;} 
       case 9:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm2.setPWM(4, 0, pulselen);}
                delay(250);
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm2.setPWM(0, 0, pulselen);
                    pwm2.setPWM(1, 0, pulselen);
                    pwm2.setPWM(2, 0, pulselen);
                    pwm2.setPWM(3, 0, pulselen);
                    pwm2.setPWM(5, 0, pulselen);
                    pwm2.setPWM(6, 0, pulselen);}
                delay(250);
                one = 9;
                break;}
  }                                                                                                
  }            
  if (dec != temperature/10){
  switch (temperature/10){
            case 0:{
                for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                    pwm1.setPWM(0, 0, pulselen);}               
                delay(250);                
                for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                    pwm1.setPWM(1, 0, pulselen);
                    pwm1.setPWM(3, 0, pulselen);
                    pwm1.setPWM(2, 0, pulselen);
                    pwm1.setPWM(4, 0, pulselen);
                    pwm1.setPWM(5, 0, pulselen);
                    pwm1.setPWM(6, 0, pulselen);}               
                delay(250);
                dec = 0;
                break;} 
     case 1:{
              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);}              
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}              
              delay(250);
              dec = 1;
              break;}   
              
     case 2:{
              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);}
              delay(250);
              dec = 2;
              break;}
     case 3:{
              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);}
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(6, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(0, 0, pulselen);}
              delay(250);
              dec = 3;
              break;}           
     case 4:{
               for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);}
              delay(250);  
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250);

              dec = 4;
              break;}
     case 5:{
              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);}
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250);
              dec = 5;
              break;}
     case 6:{
              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(3, 0, pulselen);}
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250); 
              dec = 6;            
              break;}
     case 7:{

              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);}
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250);
              dec = 7;
              break;}
     case 8:{
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(4, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250);
              dec = 8;
              break;} 
     case 9:{
              for (uint16_t pulselen = SERVOMAX; pulselen > SERVOMIN; pulselen--) {
                  pwm1.setPWM(4, 0, pulselen);}
              delay(250);
              for (uint16_t pulselen = SERVOMIN; pulselen < SERVOMAX; pulselen++) {
                  pwm1.setPWM(0, 0, pulselen);
                  pwm1.setPWM(1, 0, pulselen);
                  pwm1.setPWM(2, 0, pulselen);
                  pwm1.setPWM(3, 0, pulselen);
                  pwm1.setPWM(5, 0, pulselen);
                  pwm1.setPWM(6, 0, pulselen);}
              delay(250);
              dec = 9;
              break;}
        
  }                                                                                                
  }           

}

}
